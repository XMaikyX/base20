﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazUleam.Modelos
{
    class Login
    {
        public string Id { get; set; }
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public string Tipo { get; set; }
    }   
}
