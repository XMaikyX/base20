﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazUleam.Modelos
{
    class Tutoria
    {
        public int ID { get; set; }
        public DateTime Fecha { get; set; }
        public string Aula { get; set; }
        public string Asignatura { get; set; }
        public string Tema { get; set; }

        public void AgregarTutoria()    
        {
            Tutoria tutoria = new Tutoria();

            Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
            Console.WriteLine("Escriba el Año ");
            string anio = Console.ReadLine();
            Console.WriteLine("Escriba el Mes");
            string mes = Console.ReadLine();
            Console.WriteLine("Escriba la Hora");
            string hora = Console.ReadLine();
            DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(hora));

            Console.WriteLine("Ingrese el Aula a utilizar");
            tutoria.Aula = Console.ReadLine();
            Console.WriteLine("Ingrese la Asignatura");
            tutoria.Asignatura = Console.ReadLine();
            Console.WriteLine("Ingrese el Tema");
            tutoria.Tema = Console.ReadLine();

            Console.WriteLine("Se asignó tutoría para:");
            Console.WriteLine(this.Fecha+" "+Aula+" "+Asignatura+"\n"+Tema);
        }

    }
}
