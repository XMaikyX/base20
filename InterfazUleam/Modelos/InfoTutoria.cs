﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazUleam.Modelos
{
    class InfoTutoria
    {
        private List<Tutoria> tutorias;

        public List<Tutoria> Tutorias
        {
            get { return tutorias; }
            set { tutorias = value; }
        }
            
    }
}
