﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazUleam.Modelos
{
    class EntityFrameworkContext :DbContext
    {
        private const string connectionString =
            "Server=.\\MICHAEL; Database= InterfazUleam; Trusted_Connection= True;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Profesor> Profesors { get; set; }
        public DbSet<Estudiante> Estudiantes { get; set; }
        public DbSet<Administrador> Administradors { get; set; }
        public DbSet<Tutoria> Tutorias { get; set; }
        public DbSet<Login> Logins { get; set; }    
        public DbSet<LIstaAulas> LIstaAulas { get; set; }

    }
}
